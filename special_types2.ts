let w: unknown = 1;
w = "string";
w = {
    ronANonExistentMethod: () => {
        console.log("I think therefore I am");
    }
} as { ronANonExistentMethod: () => void}

if(typeof w == 'object' && w!==null) {
    (w as {ronANonExistentMethod: () => void}).ronANonExistentMethod();
}